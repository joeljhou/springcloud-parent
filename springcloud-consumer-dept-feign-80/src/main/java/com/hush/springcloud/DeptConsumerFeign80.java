package com.hush.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author 周宇
 * @create 2020-11-22 17:39
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages={"com.hush.springcloud"})
//@ComponentScan("com.hush.springcloud") 默认扫描启动文件下的包
public class DeptConsumerFeign80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerFeign80.class, args);
    }
}
