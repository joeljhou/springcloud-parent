package com.hush.springcloud.service.impl;

import com.hush.springcloud.pojo.Dept;
import com.hush.springcloud.dao.DeptDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 周宇
 * @create 2020-11-22 17:02
 */
@Service
public class DeptServiceImpl implements DeptService {

    //自动注入
    @Autowired
    private DeptDao deptDao;

    @Override
    public boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    @Override
    public Dept queryById(Long id) {
        return deptDao.queryById(id);
    }

    @Override
    public List<Dept> queryAll() {
        return deptDao.queryAll();
    }
}
