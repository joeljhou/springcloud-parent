package com.hush.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 周宇
 * @create 2020-11-22 17:14
 */
@SpringBootApplication
@EnableEurekaClient  //本服务启动之后会自动注册进Eureka中！
@EnableDiscoveryClient  //服务发现
public class DeptProvider8003 {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider8003.class, args);
    }
}
