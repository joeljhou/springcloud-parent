package com.hush.springcloud.dao;

import com.hush.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 周宇
 * @create 2020-11-22 16:28
 */
@Mapper
public interface DeptDao {
    boolean addDept(Dept dept); //添加一个部门

    Dept queryById(Long id); //根据id查询部门

    List<Dept> queryAll(); //查询所有部门
}
