package com.hush.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;

/**
 * 自定义规则的Rubbion类
 */
@Configurable
public class MySelfRule {

    @Bean
    public IRule myRule(){
        //return new RandomRule();  //Ribbon默认是轮询
        return new MyRandomRule();  //Ribbon默认是轮询，我们自定义为随机算法
    }

}
