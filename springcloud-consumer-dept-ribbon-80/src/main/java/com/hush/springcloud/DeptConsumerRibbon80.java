package com.hush.springcloud;

import com.hush.myrule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author 周宇
 * @create 2020-11-22 17:39
 */
//Ribbon 和 Eureka 整合后，客户端可以直接调用，不用关心IP地址和端口号~
@SpringBootApplication
@EnableEurekaClient
//核心！ 在微服務启动的时候去加载我们自定义的Ribbon类
@RibbonClient(name = "SPRINGCLOUD-PROVIDER-DEPT",configuration = MySelfRule.class)
public class DeptConsumerRibbon80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerRibbon80.class, args);
    }
}
