package com.hush.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 周宇
 * @create 2020-11-22 18:00
 */
@SpringBootApplication
@EnableEurekaServer    //@EnableEurekaServer 服务端的启动类，可以接收别人注册进来
public class EurekaServer7001 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer7001.class, args);
    }
}
