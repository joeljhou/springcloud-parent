package com.hush.springcloud.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 周宇
 * @create 2020-11-22 15:40
 */
@NoArgsConstructor
@Data
@Accessors(chain = true)  //链式编程
public class Dept implements Serializable { //一定要实现序列化
    private Long deptno;
    private String dname;
    private String db_source;

    public Dept(String dname) {
        this.dname = dname;
    }

    /*
        链式写法
        Dept dept = new Dept();
        dept.setDeptno(11L).setDname("school").setDb_source("springcloud01");
    */
}
