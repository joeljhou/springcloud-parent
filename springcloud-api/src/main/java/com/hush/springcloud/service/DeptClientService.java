package com.hush.springcloud.service;

import com.hush.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Component
@FeignClient(value = "SPRINGCLOUD-PROVIDER-DEPT")
public interface DeptClientService {

    @GetMapping("/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id); //根据id查询部门

    @PostMapping("/dept/add")
    public boolean addDept(@RequestBody Dept dept);  //查询所有部门

    @GetMapping("/dept/list")
    public List<Dept> queryAll();  //添加一个部门

}
